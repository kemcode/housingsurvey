﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace housingSurvey.Models
{
    [MetadataType(typeof(MetaUser))]
    public partial class survQuestion
    {

        public class MetaUser
        {

            [DisplayName("1. Gender ")]
            public Nullable<int> gender { get; set; }

            [DisplayName("2. What is your age group? ")]
            public Nullable<int> ageRange { get; set; }


            [DisplayName("3. Select your Nationality ")]
            public Nullable<int> nationality_FK { get; set; }
            public Nullable<int> village_FK { get; set; }


            [DisplayName("5. What is the highest degree or level or education you have completed? ")]
            public Nullable<int> eduLevel { get; set; }

            [DisplayName("6. What is your marital status? ")]
            public Nullable<int> marStatus_FK { get; set; }
            [DisplayName("7. Which group best describe your current status? ")]
            public Nullable<int> descripCap { get; set; }

            [DisplayName("8. What is your current employment status? ")]
            public Nullable<int> empStatsus { get; set; }

            [DisplayName("9. Number of dependents?(Children, elderly parents or other individuals requiring your financial support) ")]
            public Nullable<int> numDepend { get; set; }

            [DisplayName("10. How many people currently reside with you at your present accommodation? ")]
            public Nullable<int> numCurrentHome { get; set; }


            [DisplayName("11. For staatistical purposes only, please indicate your monthly take home income(after deductions). ")]
            public Nullable<int> monthIncRange { get; set; }

            [DisplayName("12. Do you have an existing mortgage or student loan? ")]
            public bool existLoan { get; set; }

            [DisplayName("13. Which of the following best describes your current housing situation? ")]
            public Nullable<int> currentHomeSit { get; set; }

            [DisplayName("14. Do you have any savings or equity which could be used towards the purchase of a home? ")]
            public Nullable<int> savingsEqu { get; set; }

            [DisplayName("15. Do you own a home? ")]
            public bool ownHome { get; set; }

            [DisplayName("16. If not owned, what has prevented you from becoming a home-owner? ")]
            [DataType(DataType.MultilineText)]
            public string noHomeWhy { get; set; }

            [DisplayName("17. What kind of payment or financing arrangement would you be interested in for this new housing initative? ")]
            public Nullable<int> financeOption { get; set; }

            [DisplayName("18. What is your total annual income and family income (if any) of the persons wishing to move with you ? ")]
            public Nullable<int> annIncome { get; set; }

            [DisplayName("19. What kind of housing enviroment would you be most interested in? ")]
            public Nullable<int> houseEnviro { get; set; }

            [DisplayName("20. Please indicate the type of dwelling structure you would be most interested in. ")]
            public Nullable<int> dwellStruct { get; set; }

            [DisplayName("21. Which of the following best describes your housing need? ")]
            public Nullable<int> bedroomNeed { get; set; }

            [DisplayName("22. What is your bathroom preference? ")]
            public Nullable<int> bathroomNeed { get; set; }

            [DisplayName("23. How many individuals are likely to be moving with you? ")]
            public Nullable<int> individualMov { get; set; }
            public bool inAmtLaundry { get; set; }
            public bool inAmtMasB { get; set; }
            public bool inAmtPantry { get; set; }
            public bool inAmtSolar { get; set; }
            public bool inAmtYard { get; set; }
            public bool inAmtGarage { get; set; }
            public bool inAmtWheel { get; set; }
            public bool inAmtBPorch { get; set; }
            public bool inAmtBase { get; set; }
            public bool outAmtPool { get; set; }
            public bool outAmtPark { get; set; }
            public bool outAmtShop { get; set; }
            public bool outAmtPubWIFI { get; set; }
            public bool outAmtWheel { get; set; }
            public bool outAmtParking { get; set; }
            public bool outAmtWash { get; set; }
            public bool outAmtTenis { get; set; }
            public bool outAmtPlay { get; set; }


            [DisplayName("26. What size lot/floor space would be preferable? ")]
            public int sizeHome { get; set; }

            [DisplayName("27. If you wish to buy a home, what total house price do you think you could afford? ")]
            public int homeAfford { get; set; }
            [DisplayName("28. Are you likely to apply for this new housing initative? ")]
            public bool likelyApply { get; set; }
            [DisplayName("If no, Why? ")]
            [DataType(DataType.MultilineText)]
            public string noApplyWhy { get; set; }
            [DisplayName("29. Is there anything that you would like to share with us regarding the Government's new housing iniative? ")]
            [DataType(DataType.MultilineText)]
            public string elseShare { get; set; }
        }
    }
}