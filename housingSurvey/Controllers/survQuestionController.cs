﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housingSurvey.Models;

namespace housingSurvey.Controllers
{
    public class survQuestionController : Controller
    {
        private housingComSurv_DBEntities db = new housingComSurv_DBEntities();

        // GET: survQuestion
        public async Task<ActionResult> Index()
        {
            var survQuestions = db.survQuestions.Include(s => s.REF_nationality).Include(s => s.REF_villages);
            return View(await survQuestions.ToListAsync());
        }

        // GET: survQuestion/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            if (survQuestion == null)
            {
                return HttpNotFound();
            }
            return View(survQuestion);
        }
        [AllowAnonymous]
        [OverrideActionFilters]
        public ActionResult Thanks()
        {
            return View();
        }
        // GET: survQuestion/Create
        public ActionResult Create()
        {
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality");
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME").OrderBy(x=>x.Text);
            return View();
        }

        // POST: survQuestion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "questionId,gender,ageRange,nationality_FK,village_FK,eduLevel,marStatus_FK,descripCap,empStatsus,numDepend,numCurrentHome,monthIncRange,existLoan,currentHomeSit,savingsEqu,ownHome,noHomeWhy,financeOption,annIncome,houseEnviro,dwellStruct,bedroomNeed,bathroomNeed,individualMov,inAmtLaundry,inAmtMasB,inAmtPantry,inAmtSolar,inAmtYard,inAmtGarage,inAmtWheel,inAmtBPorch,inAmtBase,outAmtPool,outAmtPark,outAmtShop,outAmtPubWIFI,outAmtWheel,outAmtParking,outAmtWash,outAmtTenis,outAmtPlay,sizeHome,homeAfford,likelyApply,noApplyWhy,elseShare")] survQuestion survQuestion)
        {
            if (ModelState.IsValid)
            {
                db.survQuestions.Add(survQuestion);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality", survQuestion.nationality_FK);
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME", survQuestion.village_FK);
            return View(survQuestion);
        }

        // GET: survQuestion/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            if (survQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality", survQuestion.nationality_FK);
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME", survQuestion.village_FK);
            return View(survQuestion);
        }

        // POST: survQuestion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "questionId,gender,ageRange,nationality_FK,village_FK,eduLevel,marStatus_FK,descripCap,empStatsus,numDepend,numCurrentHome,monthIncRange,existLoan,currentHomeSit,savingsEqu,ownHome,noHomeWhy,financeOption,annIncome,houseEnviro,dwellStruct,bedroomNeed,bathroomNeed,individualMov,inAmtLaundry,inAmtMasB,inAmtPantry,inAmtSolar,inAmtYard,inAmtGarage,inAmtWheel,inAmtBPorch,inAmtBase,outAmtPool,outAmtPark,outAmtShop,outAmtPubWIFI,outAmtWheel,outAmtParking,outAmtWash,outAmtTenis,outAmtPlay,sizeHome,homeAfford,likelyApply,noApplyWhy,elseShare")] survQuestion survQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(survQuestion).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality", survQuestion.nationality_FK);
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME", survQuestion.village_FK);
            return View(survQuestion);
        }

        // GET: survQuestion/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            if (survQuestion == null)
            {
                return HttpNotFound();
            }
            return View(survQuestion);
        }

        // POST: survQuestion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            db.survQuestions.Remove(survQuestion);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpPost]
        [AllowAnonymous]
        [OverrideActionFilters]
        public ActionResult SaveQuestions(survQuestion con_consultantinfo, HttpPostedFileBase testResults, HttpPostedFileBase medReport, HttpPostedFileBase vacProof)
        {
            try
            {
                //  Quest_details appD = con_consultantinfo.id > 0 ? db.Quest_details.FirstOrDefault(p => p.formID == con_consultantinfo.formID) : new Quest_details();
                survQuestion appD = new survQuestion();
                //if (con_consultantinfo.consultID == null)
                //{



 
 

                // -----------------------------------------

             //   appD.gender = gender;

 
                appD.gender = con_consultantinfo.gender;
                appD.ageRange = con_consultantinfo.ageRange;
                appD.nationality_FK = con_consultantinfo.nationality_FK;
                appD.village_FK = con_consultantinfo.village_FK;
                appD.eduLevel = con_consultantinfo.eduLevel;
                appD.marStatus_FK = con_consultantinfo.marStatus_FK;
                appD.descripCap = con_consultantinfo.descripCap;
                appD.empStatsus = con_consultantinfo.empStatsus;
                appD.numDepend = con_consultantinfo.numDepend;
                appD.numCurrentHome = con_consultantinfo.numCurrentHome;
                appD.monthIncRange = con_consultantinfo.monthIncRange;
                appD.existLoan = con_consultantinfo.existLoan;
                appD.currentHomeSit = con_consultantinfo.currentHomeSit;
                appD.savingsEqu = con_consultantinfo.savingsEqu;
                appD.ownHome = con_consultantinfo.ownHome;
                appD.noHomeWhy = con_consultantinfo.noHomeWhy;
                appD.financeOption = con_consultantinfo.financeOption;
                appD.annIncome = con_consultantinfo.annIncome;
                appD.houseEnviro = con_consultantinfo.houseEnviro;
                appD.dwellStruct = con_consultantinfo.dwellStruct;
                appD.bedroomNeed = con_consultantinfo.bedroomNeed;
                appD.bathroomNeed = con_consultantinfo.bathroomNeed;

                appD.individualMov = con_consultantinfo.individualMov;

                appD.inAmtLaundry = con_consultantinfo.inAmtLaundry;
                appD.inAmtMasB = con_consultantinfo.inAmtMasB;
                appD.inAmtPantry = con_consultantinfo.inAmtPantry;
                appD.inAmtSolar = con_consultantinfo.inAmtSolar;
                appD.inAmtYard = con_consultantinfo.inAmtYard;
                appD.inAmtGarage = con_consultantinfo.inAmtGarage;
                appD.inAmtWheel = con_consultantinfo.inAmtWheel;
                appD.inAmtBPorch = con_consultantinfo.inAmtBPorch;
                appD.inAmtBase = con_consultantinfo.inAmtBase;

                appD.outAmtPool = con_consultantinfo.outAmtPool;
                appD.outAmtPark = con_consultantinfo.outAmtPark;
                appD.outAmtShop = con_consultantinfo.outAmtShop;
                appD.outAmtPubWIFI = con_consultantinfo.outAmtPubWIFI;
                appD.outAmtWheel = con_consultantinfo.outAmtWheel;
                appD.outAmtParking = con_consultantinfo.outAmtParking; 
                appD.outAmtWash = con_consultantinfo.outAmtWash;
                appD.outAmtTenis = con_consultantinfo.outAmtTenis;
                appD.outAmtPlay = con_consultantinfo.outAmtPlay;
                appD.sizeHome = con_consultantinfo.sizeHome;
                appD.homeAfford = con_consultantinfo.homeAfford;
                appD.likelyApply = con_consultantinfo.likelyApply;

                appD.dateCreated = DateTime.Now;
                appD.status_fk = 1;

                appD.noApplyWhy = con_consultantinfo.noApplyWhy;
                appD.elseShare = con_consultantinfo.elseShare;
 



                db.Entry(appD).State = EntityState.Added;
                db.SaveChanges();

                //countryVisit
 

                return Json(new
                {
                    Result = true,
                  //  Name = appD.firstName + " " + appD.lastName,
                    NowDate = DateTime.Now//,
                    //PassId = appD.formID
                  //  PassId = appD.formID
                });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
