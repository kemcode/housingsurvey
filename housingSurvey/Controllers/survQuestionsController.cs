﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housingSurvey.Models;

namespace housingSurvey.Controllers
{
    public class survQuestionsController : Controller
    {
        private housingComSurv_DBEntities db = new housingComSurv_DBEntities();

        // GET: survQuestions
        public async Task<ActionResult> Index()
        {
            var survQuestions = db.survQuestions.Include(s => s.REF_nationality).Include(s => s.REF_villages);
            return View(await survQuestions.ToListAsync());
        }

        // GET: survQuestions/Details/5
        public async Task<ActionResult> Details(bool? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            if (survQuestion == null)
            {
                return HttpNotFound();
            }
            return View(survQuestion);
        }

        // GET: survQuestions/Create
        public ActionResult Create()
        {
            ViewBag.marStatus_FK = new SelectList(db.REF_marStatus, "id", "descrip");
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality");
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME");
            return View();
        }
        // GET: survQuestions/Create
        public ActionResult Createb()
        {
            ViewBag.marStatus_FK = new SelectList(db.REF_marStatus, "id", "descrip");
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality");
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME");
            return View();
        }
        // POST: survQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "gender,ageRange,nationality_FK,village_FK,eduLevel,marStatus_FK,descripCap,empStatsus,numDepend,numCurrentHome,monthIncRange,existLoan,currentHomeSit,savingsEqu,ownHome,noHomeWhy,financeOption,annIncome,houseEnviro,dwellStruct,bedroomNeed,bathroomNeed,individualMov,inAmtLaundry,inAmtMasB,inAmtPantry,inAmtSolar,inAmtYard,inAmtGarage,inAmtWheel,inAmtBPorch,inAmtBase,outAmtPool,outAmtPark,outAmtShop,outAmtPubWIFI,outAmtWheel,outAmtParking,outAmtWash,outAmtTenis,sizeHome,homeAfford,likelyApply,noApplyWhy,elseShare")] survQuestion survQuestion)
        {
            if (ModelState.IsValid)
            {
                db.survQuestions.Add(survQuestion);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.marStatus_FK = new SelectList(db.REF_marStatus, "id", "descrip", survQuestion.marStatus_FK);
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality", survQuestion.nationality_FK);
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME", survQuestion.village_FK);
            return View(survQuestion);
        }

        // GET: survQuestions/Edit/5
        public async Task<ActionResult> Edit(bool? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            if (survQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.marStatus_FK = new SelectList(db.REF_marStatus, "id", "descrip", survQuestion.marStatus_FK);
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality", survQuestion.nationality_FK);
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME", survQuestion.village_FK);
            return View(survQuestion);
        }

        // POST: survQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "gender,ageRange,nationality_FK,village_FK,eduLevel,marStatus_FK,descripCap,empStatsus,numDepend,numCurrentHome,monthIncRange,existLoan,currentHomeSit,savingsEqu,ownHome,noHomeWhy,financeOption,annIncome,houseEnviro,dwellStruct,bedroomNeed,bathroomNeed,individualMov,inAmtLaundry,inAmtMasB,inAmtPantry,inAmtSolar,inAmtYard,inAmtGarage,inAmtWheel,inAmtBPorch,inAmtBase,outAmtPool,outAmtPark,outAmtShop,outAmtPubWIFI,outAmtWheel,outAmtParking,outAmtWash,outAmtTenis,sizeHome,homeAfford,likelyApply,noApplyWhy,elseShare")] survQuestion survQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(survQuestion).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.marStatus_FK = new SelectList(db.REF_marStatus, "id", "descrip", survQuestion.marStatus_FK);
            ViewBag.nationality_FK = new SelectList(db.REF_nationality, "ID", "Nationality", survQuestion.nationality_FK);
            ViewBag.village_FK = new SelectList(db.REF_villages, "id", "CITY_NAME", survQuestion.village_FK);
            return View(survQuestion);
        }

        // GET: survQuestions/Delete/5
        public async Task<ActionResult> Delete(bool? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            if (survQuestion == null)
            {
                return HttpNotFound();
            }
            return View(survQuestion);
        }

        // POST: survQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(bool id)
        {
            survQuestion survQuestion = await db.survQuestions.FindAsync(id);
            db.survQuestions.Remove(survQuestion);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
